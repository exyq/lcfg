#!/bin/bash
# Normal init File
rm ~/.tmux* -r

cp ~/lcfg/.tmux.conf ~/ 
tar -xzvf ~/lcfg/.tmux.tar.gz -C ~/

tmux source ~/.tmux.conf

sh ~/lcfg/vim/viminit.sh

echo "DISABLE_AUTO_UPDATE=\"true\"" >> ~/.zshrc


