#!/bin/bash
# First init File
cp ~/lcfg/.tmux.conf ~/ 
tar -xzvf ~/lcfg/.tmux.tar.gz -C ~/

tmux source ~/.tmux.conf

sh ~/lcfg/ohmyzsh/tools/install.sh

sh ~/lcfg/vim/viminit.sh

cat ~/lcfg/shrc >> ~/.zshrc
cat ~/lcfg/shrc >> ~/.ashrc
cat ~/lcfg/shrc >> ~/.bashrc



