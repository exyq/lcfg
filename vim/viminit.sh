#!/bin/bash
sudo mkdir ~/.vim/view -p
sudo cp ~/lcfg/vim/_vimrc ~/.vimrc
sudo cp -r ~/lcfg/vim/vplug/plug.vim /usr/share/vim/vim*/autoload/
sudo cp -r ~/lcfg/vim/vplug /usr/share/vim/
sudo cp -r ~/lcfg/vim/.vim /usr/share/vim/
